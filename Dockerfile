FROM node:16.13.2-slim
ARG SSH_KEY
RUN apt-get update && \
    apt-get install -y \
        git \
        openssh-server 

# Authorize SSH Host
RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    ssh-keyscan gitlab.com > /root/.ssh/known_hosts

# Add the keys and set permissions
RUN echo "$SSH_KEY" | base64 --decode > /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa
WORKDIR /usr/src/app
COPY package.json .
COPY .npmrc .npmrc
RUN npm install
COPY . .
RUN rm .npmrc
# EXPOSE 3001
# ENTRYPOINT [ "npm", "run", "start"]
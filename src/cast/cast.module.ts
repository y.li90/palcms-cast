import { Module } from '@nestjs/common';
import { CastService } from './cast.service';
import { CastController } from './cast.controller';
import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';


@Module({
  imports: [ConfigModule, HttpModule],
  providers: [CastService],
  controllers: [CastController],
  exports: [CastService],

})
export class CastModule {}

import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';
const { createHash, createCipheriv } = require('crypto');
const {
  models: { Passcode },
} = require('@palvisioncorp/palcms-model');


const roomNo = 602
// const switchIp = 'http://192.168.1.100'
const switchIp = 'http://58.185.234.62:8080/'
const ipAddress = '127.0.0.1'
const deviceIpAddress = '127.0.0.1'
let aclId;
let cookie;


@Injectable()
export class CastService {
  constructor(
    private configService: ConfigService,
    private httpService: HttpService
  ) {}


//here

  async logout() {
    console.log('try logout');
    const result = await this.httpService.delete(`${switchIp}/rest/v1/login-sessions`,
      {
        headers: { Cookie: cookie }, 
        data: {
          userName: "admin",
          password: "P@lv1s10n"
        }
      }).toPromise()
//	console.log('logout', result.statusCode);
    cookie = result.data.cookie
  }

  async  deleteAcl({ sourceIp }) {
    console.log(`deleting ${switchIp} -- ${aclId} -- ${roomNo} -- ${cookie}`)
    const result = await this.httpService.delete(`${switchIp}/rest/v1/acls/${aclId}/rules/${roomNo}`, {
      headers: {
        'Cookie': cookie
      }
    }).toPromise()
    console.log('delete', result.data);
    await this.updateAcl({sourceIp});
  }

  async updateAcl({sourceIp}) {
    const result = await this.httpService.post(`${switchIp}/rest/v1/acls/${aclId}/rules`, {
      sequence_no: roomNo,
      acl_id: aclId,
      acl_action: "AA_PERMIT",
      traffic_match: {
        protocol_type: "PT_IP",
        source_ip_address: {
          version: "IAV_IP_V4",
          octets: sourceIp
        },
        source_ip_mask: {
          version: "IAV_IP_V4",
          octets: "0.0.0.0",
        },
        destination_ip_address: {
          version: "IAV_IP_V4",
          octets: deviceIpAddress
        },
        destination_ip_mask: {
          version: "IAV_IP_V4",
          octets: "0.0.0.0"
        }
      }
    }, {
      headers: {
        'Cookie': cookie
      }
    }).toPromise()
    console.log('update', result.data);
    await this.logout()

  }

  async checkAcl({ sourceIp }) {
    const result = await this.httpService.get(`${switchIp}/rest/v1/acls`, {
      headers: {
        'Cookie': cookie
      }
    }).toPromise()
    console.log('check acl', result.data);
    aclId = result.data.acl_element[0].id
    await this.deleteAcl({ sourceIp });

  }


  async aclLogin({sourceIp}) {
    console.log('try login');
    const result = await this.httpService.post(`${switchIp}/rest/v1/login-sessions`, {
      userName: "admin",
      password: "P@lv1s10n"
    }).toPromise()
    console.log('login', result.data);
    cookie = result.data.cookie
    await this.checkAcl({sourceIp});
          //await logout()

  }

  hash(string) {
    return createHash('sha256').update(string).digest('hex');
  }
  aesEncrypt(toEncrypt, key, iv){
    console.log('to', toEncrypt)
    console.log('key', key);
    console.log('iv', iv);

    const hashKey = createHash('sha256');
    hashKey.update(key);
    key = hashKey.digest('hex').substring(0, 32);
      
    const hashIv = createHash('sha256');
    hashIv.update(iv);
    iv = hashIv.digest('hex').substring(0, 16);
    console.log('key', key, iv);
    
    const cipher = createCipheriv('AES-256-CBC', key, iv);
    console.log('here', cipher);
    let encrypted = cipher.update(toEncrypt, 'utf8', 'base64'); // 1st Base64 encoding
    console.log('encrypted', encrypted)
    encrypted += cipher.final('base64'); 
    console.log('enc 2', encrypted);
    return Buffer.from(encrypted, 'utf8').toString('base64'); // 2nd Base64 encoding
  };

  //end
  async encryptString(data) {
    // try {
      // let string = `room_no=${roomNo}&ip_address=${data.ipAddress}&device_name=Local`;
      console.log('data', data);
      const { roomNo, ipAddress } = data
      let string = `room_no=${roomNo}&ip_address=${ipAddress}&device_name=Server`

      const privateKey 	= '59IRJAXAT3HNSNG5KKPXBCZ265X4'; // user define key
      const secretKey 		= '443HTLOZI78'; // user define secret key
      const encryptMethod  = "AES-256-CBC";

      // let key    = this.hash(privateKey);
      // let ivalue = this.hash(secretKey).slice(0,16); // sha256 is hash_hmac_algo
      let result = this.aesEncrypt(string, privateKey, secretKey)
      console.log('res', result);

      let qrCodeContent = `http://roomcastv.com/connect?i=${result}`;
      console.log('qr', qrCodeContent);
      return {
          "status": 200,
          "error": false,
          "error_message": "",
          "request": [],
          "response": {
              "url": qrCodeContent
          }
      };

    // }
    // catch (e) {
    //   return e
    // }





  }
  async getQrCode({ roomNo, ipAddress}) {
    console.log('ip is ', ipAddress)
    return this.encryptString({roomNo, ipAddress})
  }

  async getPassCode({ roomNo, checkoutDate, deviceName }) {
    const passcode = Math.floor(Math.random() * 100000000);

    const newPasscode = new Passcode({
      passcode,
      roomNo,
      checkoutDate,
      deviceName
    });
    return await newPasscode.save();
  }


  async validatePassCode({ passcode }) {
    let result = await Passcode.findOne({ passcode });
    console.log('result', result);
    if(result) {
      return result
    } else {
      return "Wrong passcode"
    }
  }

  async castAuth({ roomNo }) {
    const sourceIp = '127.0.0.1'
    await this.aclLogin({sourceIp})
    return "Ok"
  }
}

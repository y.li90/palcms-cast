import { Body, Controller, Post, Get, UseGuards, UseInterceptors, UploadedFiles, Req } from '@nestjs/common';
import { IntegrationChannelDec } from '@palvisioncorp/palcms-service';
import { FilesInterceptor } from '@nestjs/platform-express';
import { CastService } from './cast.service';
import {
  RestAuthGuard,
  TransformInterceptor
} from '@palvisioncorp/palcms-service';
import { diskStorage } from 'multer';

@Controller('cast')
export class CastController {
  constructor(private readonly castService: CastService) {}

  @Post('/cast-auth')
  async castAuth(
    @Body() data: any,
  ) {
    const { roomNo } = data;
    const input = await this.castService.castAuth({ roomNo });
    return {
      data: input,
    };
  }

  @Get('/get-qr-code')
  async getQrCode(
    @Req() req: any,
    @Body() data: any,

  ) {
    const { roomNo } = data;

    console.log('req', req.connection.remoteAddress)
    const input = await this.castService.getQrCode({roomNo, ipAddress: req.connection.remoteAddress});
    return input;
  }

  @Post('/get-pass-code')
  @UseInterceptors(TransformInterceptor)
  async getPassCode(
    @Body() data: any,
  ) {
    const { roomNo, deviceName, checkoutDate } = data;
    const input = await this.castService.getPassCode({ roomNo, deviceName, checkoutDate });
    return input;
  }

  @Post('/validate-pass-code')
  @UseInterceptors(TransformInterceptor)
  async validatePassCode(
    @Body() data: any,
  ) {
    const { passcode } = data;
    const input = await this.castService.validatePassCode({ passcode });
    return input;
  }

  

  

}

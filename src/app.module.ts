import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from '@palvisioncorp/palcms-service';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CastModule } from './cast/cast.module';
@Module({
  imports: [
    ConfigModule,
    AuthModule,
    CastModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

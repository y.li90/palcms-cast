import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { json, urlencoded } from 'express';
// import { graphqlUploadExpress } from 'graphql-upload';
import { AppModule } from './app.module';

import { connect } from '@palvisioncorp/palcms-model';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  const configService: ConfigService = app.get(ConfigService);
  await connect(
    `${configService.get<string>('ENV') == 'prod' ? 'mongodb' : 'mongodb+srv'}://${configService.get<string>(
      'DB_USER',
    )}:${configService.get<string>('DB_PASSWORD')}@${configService.get<string>(
      'DB_HOST',
    )}/${configService.get<string>('DB_NAME')}`,
  );
  app.setGlobalPrefix('palcms-cast');
  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));

  await app.listen(3125);
}
bootstrap();
